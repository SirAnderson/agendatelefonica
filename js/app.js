var app = angular.module("agendaTelefonica",[]);
app.controller("listaTelefonicaCtrl", function($scope){
	$scope.app = "Lista Telefonica";
	$scope.contatos =[
	{nome: "Pedro", telefone: "99984548", operadora:{nome: "Vivo", codigo:"15"}},
	{nome: "Ana", telefone: "991584965", operadora:{nome: "Tim", codigo:"41"}},
	{nome: "Maria", telefone: "988469595", operadora:{nome: "Oi", codigo:"14"}},
	{nome: "Marco", telefone: "981569987", operadora:{nome: "Claro", codigo:"21"}}
	];
});

/*
operadora_nome:"Vivo", operadora_codigo:"15"
operadora_nome:"Tim", operadora_codigo:"41"
operadora_nome:"Oi", operadora_codigo:"14"
operadora_nome:"Claro", operadora_codigo:"21"
*/